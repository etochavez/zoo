<?php

namespace Zoo\Data\Test\Unit\Domain;

use Zoo\Data\Domain\Animal;
use Zoo\Data\Domain\Monkey;
use PHPUnit\Framework\TestCase;

class MonkeyTest extends TestCase
{
    private Monkey $monkey;

    protected function setUp(): void
    {
        $this->monkey = new Monkey();
    }

    /** @test */
    public function decreaseMonkeyHealth(): void
    {
        $this->monkey->getOld();

        $random_number = Animal::MAX_HEALTH - $this->monkey->health;

        $this->assertThat(
            $random_number,
            $this->logicalAnd(
                $this->greaterThanOrEqual(Animal::MIN_HEALTH_DECREASE),
                $this->lessThanOrEqual(Animal::MAX_HEALTH_DECREASE)
            )
        );
    }

    /** @test */
    public function increaseMonkeyHealth(): void
    {
        $old_health = Monkey::MIN_MONKEY_TO_STAY_ALIVE;
        $this->monkey->health = $old_health;

        $this->monkey->getFood(rand(Animal::MIN_HEALTH_INCREASE, Animal::MAX_HEALTH_INCREASE));

        $random_number = $this->monkey->health - $old_health;

        $this->assertThat(
            $random_number,
            $this->logicalAnd(
                $this->greaterThanOrEqual(Animal::MIN_HEALTH_INCREASE),
                $this->lessThanOrEqual(Animal::MAX_HEALTH_INCREASE)
            )
        );
    }

    /** @test */
    public function monkeyCantHaveMoreHealthThanMax(): void
    {
        $this->monkey->getFood(rand(Animal::MIN_HEALTH_INCREASE, Animal::MAX_HEALTH_INCREASE));

        $this->assertLessThanOrEqual(Animal::MAX_HEALTH, $this->monkey->health);
    }

    /** @test */
    public function ifMonkeyHealthLessThanMinDead(): void
    {
        $this->monkey->health = Monkey::MIN_MONKEY_TO_STAY_ALIVE - 1;
        $this->monkey->walk = false;

        $this->monkey->status();

        $this->assertTrue($this->monkey->isDead);
    }
}