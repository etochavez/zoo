<?php

namespace Zoo\Data\Test\Unit\Domain;

use Zoo\Data\Domain\Animal;
use Zoo\Data\Domain\Elephant;
use PHPUnit\Framework\TestCase;

class ElephantTest extends TestCase
{
    private Elephant $elephant;

    protected function setUp(): void
    {
        $this->elephant = new Elephant();
    }

    /** @test */
    public function decreaseElephantHealth(): void
    {
        $this->elephant->getOld();

        $random_number = Animal::MAX_HEALTH - $this->elephant->health;

        $this->assertTrue($this->elephant->walk);

        $this->assertThat(
            $random_number,
            $this->logicalAnd(
                $this->greaterThanOrEqual(Animal::MIN_HEALTH_DECREASE),
                $this->lessThanOrEqual(Animal::MAX_HEALTH_DECREASE)
            )
        );
    }

    /** @test */
    public function increaseElephantHealth(): void
    {
        $old_health = Elephant::MIN_ELEPHANT_TO_STAY_ALIVE;
        $this->elephant->health = $old_health;

        $this->elephant->getFood(rand(Animal::MIN_HEALTH_INCREASE, Animal::MAX_HEALTH_INCREASE));

        $random_number = $this->elephant->health - $old_health;

        $this->assertThat(
            $random_number,
            $this->logicalAnd(
                $this->greaterThanOrEqual(Animal::MIN_HEALTH_INCREASE),
                $this->lessThanOrEqual(Animal::MAX_HEALTH_INCREASE)
            )
        );
    }

    /** @test */
    public function elephantCantHaveMoreHealthThanMax(): void
    {
        $this->elephant->getFood(rand(Animal::MIN_HEALTH_INCREASE, Animal::MAX_HEALTH_INCREASE));

        $this->assertLessThanOrEqual(Animal::MAX_HEALTH, $this->elephant->health);
    }

    /** @test */
    public function ifHealthLessThanMinForSecondTimeDead(): void
    {
        $this->elephant->health = Elephant::MIN_ELEPHANT_TO_STAY_ALIVE - 1;
        $this->elephant->walk = false;

        $this->elephant->status();

        $this->assertTrue($this->elephant->isDead);
    }

    /** @test */
    public function ifHealthLessThanMinForFirstTimeCantWalkAndAlive(): void
    {
        $this->elephant->health = Elephant::MIN_ELEPHANT_TO_STAY_ALIVE - 1;
        $this->elephant->walk = 'Yes';

        $this->elephant->status();

        $this->assertFalse($this->elephant->walk);
        $this->assertFalse($this->elephant->isDead);
    }

}