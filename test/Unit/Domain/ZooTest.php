<?php

namespace Zoo\Data\Test\Unit\Domain;

use PHPUnit\Framework\TestCase;
use Zoo\Data\Domain\Factory\AnimalFactory;
use Zoo\Data\Domain\Zoo;

class ZooTest extends TestCase
{
    /** @test */
    public function zooCanBeCreated()
    {
        $creationArray = [
            'Elephant', 'Elephant',
            'Monkey', 'Monkey',
            'Giraffe', 'Giraffe'
        ];
        $zoo = new Zoo();
        $animals = $zoo->createZoo($creationArray);

        $expected_result = [
            AnimalFactory::createAnimal('Elephant'), AnimalFactory::createAnimal('Elephant'),
            AnimalFactory::createAnimal('Monkey'), AnimalFactory::createAnimal('Monkey'),
            AnimalFactory::createAnimal('Giraffe'), AnimalFactory::createAnimal('Giraffe')
        ];
        $this->assertEquals($expected_result, $animals);
    }

}