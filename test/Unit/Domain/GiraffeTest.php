<?php

namespace Zoo\Data\Test\Unit\Domain;

use Zoo\Data\Domain\Animal;
use Zoo\Data\Domain\Giraffe;
use PHPUnit\Framework\TestCase;

class GiraffeTest extends TestCase
{
    private Giraffe $giraffe;

    protected function setUp(): void
    {
        parent::setUp();
        $this->giraffe = new Giraffe();
    }

    /** @test */
    public function decreaseGiraffeHealth(): void
    {
        $this->giraffe->getOld();

        $random_number = Animal::MAX_HEALTH - $this->giraffe->health;

        $this->assertThat(
            $random_number,
            $this->logicalAnd(
                $this->greaterThanOrEqual(Animal::MIN_HEALTH_DECREASE),
                $this->lessThanOrEqual(Animal::MAX_HEALTH_DECREASE)
            )
        );
    }

    /** @test */
    public function increaseGiraffeHealth(): void
    {
        $old_health = Giraffe::MIN_GIRAFFE_TO_STAY_ALIVE;
        $this->giraffe->health = $old_health;

        $this->giraffe->getFood(rand(Animal::MIN_HEALTH_INCREASE, Animal::MAX_HEALTH_INCREASE));

        $random_number = $this->giraffe->health - $old_health;

        $this->assertThat(
            $random_number,
            $this->logicalAnd(
                $this->greaterThanOrEqual(Animal::MIN_HEALTH_INCREASE),
                $this->lessThanOrEqual(Animal::MAX_HEALTH_INCREASE)
            )
        );
    }

    /** @test */
    public function giraffeCantHaveMoreHealthThanMax(): void
    {
        $this->giraffe->getFood(rand(Animal::MIN_HEALTH_INCREASE, Animal::MAX_HEALTH_INCREASE));

        $this->assertLessThanOrEqual(Animal::MAX_HEALTH, $this->giraffe->health);
    }

    /** @test */
    public function ifGiraffeHealthLessThanMinDead(): void
    {
        $this->giraffe->health = Giraffe::MIN_GIRAFFE_TO_STAY_ALIVE - 1;
        $this->giraffe->walk = false;

        $this->giraffe->status();

        $this->assertTrue($this->giraffe->isDead);
    }

}