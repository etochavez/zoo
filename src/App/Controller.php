<?php declare(strict_types=1);

namespace Zoo\Data\App;

use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;
use Zoo\Data\Domain\Zoo;

class Controller
{
    private Environment $tiwg_template;

    public function __construct()
    {
        $this->tiwg_template = new Environment(new \Twig\Loader\FilesystemLoader('/var/www/html/src/Presentation'));
    }

    public function index(ServerRequestInterface $request): ResponseInterface
    {
        $zoo = new Zoo();
        $animals = $zoo->createZoo(Zoo::getAnimalsForZoo());

        $response = new Response;
        $response->getBody()->write($this->tiwg_template->render('index.html', ['animals' => $animals]));
        return $response;
    }

    public function action(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();

        $names = $body['name'];
        $healths = $body['health'];
        $many_dead = $body['dead'];
        $walks = $body['walk'];
        $feed = array_key_exists('feed', $body);
        $time = array_key_exists('time', $body);

        $zoo = new Zoo();
        $animals = $zoo->operateZoo($names, $healths, $many_dead, $walks, $feed, $time);

        $response = new Response;
        $response->getBody()->write($this->tiwg_template->render('index.html', ['animals' => $animals]));
        return $response;
    }

}