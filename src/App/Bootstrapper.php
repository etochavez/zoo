<?php declare(strict_types=1);

namespace Zoo\Data\App;

use Laminas\Diactoros\ServerRequestFactory;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use League\Route\Router;

class Bootstrapper
{
    public static function bootstrapRequest() : void
    {
        self::getRoutes();
    }

    private static function getRoutes()
    {
        $request = ServerRequestFactory::fromGlobals(
            $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
        );

        $router = new Router;

        $router->map('GET', '/', 'Zoo\Data\App\Controller::index');

        $router->map('POST', '/', 'Zoo\Data\App\Controller::action');

        $response = $router->dispatch($request);

        (new SapiEmitter)->emit($response);

    }

}