<?php declare(strict_types=1);

namespace Zoo\Data\Domain;

class Elephant extends Animal
{
    public const MIN_ELEPHANT_TO_STAY_ALIVE = 70;

    public function __construct()
    {
        parent::__construct('Elephant');
    }

    public function status(): void
    {
        if ($this->health < self::MIN_ELEPHANT_TO_STAY_ALIVE && $this->walk) {
            $this->walk = false;
        } elseif ($this->health < self::MIN_ELEPHANT_TO_STAY_ALIVE && !$this->walk) {
            $this->isDead = true;
        } else {
            $this->walk = true;
        }
    }
}