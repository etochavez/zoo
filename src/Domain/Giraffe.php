<?php declare(strict_types=1);

namespace Zoo\Data\Domain;

class Giraffe extends Animal
{
    public const MIN_GIRAFFE_TO_STAY_ALIVE = 50;


    public function __construct()
    {
        parent::__construct('Giraffe');
    }

    public function status(): Void
    {
        if ($this->health < self::MIN_GIRAFFE_TO_STAY_ALIVE) {
            $this->isDead = true;
            $this->walk = false;
        }
    }
}