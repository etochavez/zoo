<?php declare(strict_types=1);

namespace Zoo\Data\Domain\Interface;

interface CanGetOldInterface
{
    public function getOld(): void;
}