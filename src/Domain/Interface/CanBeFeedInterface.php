<?php declare(strict_types=1);

namespace Zoo\Data\Domain\Interface;

interface CanBeFeedInterface
{
    public function getFood(int $food) : void;
}