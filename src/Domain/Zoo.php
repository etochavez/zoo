<?php declare(strict_types=1);

namespace Zoo\Data\Domain;

use Zoo\Data\Domain\Factory\AnimalFactory;
use Zoo\Data\Domain\Interface\CanBeFeedInterface;

class Zoo
{

    public function createZoo(array $creationArray): array
    {
        $animals = [];

        foreach ($creationArray as $animalName){
            $animals[] = AnimalFactory::createAnimal($animalName);
        }
        return $animals;
    }

    public function operateZoo(array $names, array $healths, array $many_dead, array $walks, $feed, $time): array
    {
        $animals = [];

        $portions = self::foodPortionPerAnimal();

        foreach ($names as $key => $animalName) {
            $animal = AnimalFactory::createAnimal($animalName);
            $animal = self::updateAnimalInfo($animal, $healths[$key], $many_dead[$key], $walks[$key]);

            $this->actionInAnimals($animal, $portions, $time, $feed);

            $animal->status();
            $animals[] = $animal;
        }

        return $animals;
    }

    public static function getAnimalsForZoo()
    {
        return [
            'Elephant', 'Elephant', 'Elephant', 'Elephant', 'Elephant',
            'Monkey', 'Monkey', 'Monkey', 'Monkey', 'Monkey',
            'Giraffe', 'Giraffe', 'Giraffe', 'Giraffe', 'Giraffe'
        ];
    }

    private static function feedAnimals(CanBeFeedInterface $animal, $portions): void
    {
        if (!$animal->isDead){
            $animal->getFood($portions[$animal->name]);
        }
    }

    private static function runTime(Animal $animal) : void
    {
        if (!$animal->isDead) {
            $animal->getOld();
        }
    }

    private static function foodPortionPerAnimal(): array
    {
        return [
            'Elephant'  => rand(Animal::MIN_HEALTH_INCREASE, Animal::MAX_HEALTH_INCREASE),
            'Giraffe'   => rand(Animal::MIN_HEALTH_INCREASE, Animal::MAX_HEALTH_INCREASE),
            'Monkey'    => rand(Animal::MIN_HEALTH_INCREASE, Animal::MAX_HEALTH_INCREASE)
        ];
    }

    private function actionInAnimals(Animal $animal, array $portions, bool $time, bool $feed): void
    {
        if ($time) {
            self::runTime($animal);
        }

        if ($feed) {
            self::feedAnimals($animal, $portions);
        }
    }

    private static function updateAnimalInfo(Animal $animal, $health, $dead, $walk): Animal
    {
        $animal->health = floatval($health);
        $animal->isDead = $dead === '1';
        $animal->walk = $walk === '1';
        return $animal;
    }
}