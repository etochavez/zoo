<?php declare(strict_types=1);

namespace Zoo\Data\Domain\Factory;

use Zoo\Data\Domain\Animal;

class AnimalFactory
{
    public static function createAnimal($name): Animal
    {
        $entity = "Zoo\Data\Domain\\" . $name;
        return new $entity();
    }
}