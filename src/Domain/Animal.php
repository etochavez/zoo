<?php declare(strict_types=1);

namespace Zoo\Data\Domain;

use Zoo\Data\Domain\Interface\CanBeFeedInterface;
use Zoo\Data\Domain\Interface\CanGetOldInterface;
use Zoo\Data\Domain\Trait\BeFeedTrait;
use Zoo\Data\Domain\Trait\GetOldTrait;

abstract class Animal implements CanGetOldInterface, CanBeFeedInterface
{
    use GetOldTrait;
    use BeFeedTrait;

    public float $health;
    const MAX_HEALTH = 100;
    const MIN_HEALTH_DECREASE = 0;
    const MAX_HEALTH_DECREASE = 20;
    const MIN_HEALTH_INCREASE = 10;
    const MAX_HEALTH_INCREASE = 25;
    public bool $walk;
    public bool $isDead;

    public function __construct(public String $name)
    {
        $this->health = self::MAX_HEALTH;
        $this->isDead = false;
        $this->walk = true;
    }

    public function getName(): string
    {
        return $this->name;
    }

    abstract public function status(): void;
}