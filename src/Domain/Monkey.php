<?php declare(strict_types=1);

namespace Zoo\Data\Domain;

class Monkey extends Animal
{
    public const MIN_MONKEY_TO_STAY_ALIVE = 30;

    public function __construct()
    {
        parent::__construct('Monkey');
    }

    public function status(): void
    {
        if ($this->health < self::MIN_MONKEY_TO_STAY_ALIVE) {
            $this->isDead = true;
            $this->walk = false;
        }
    }
}