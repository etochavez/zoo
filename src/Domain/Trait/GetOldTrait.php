<?php declare(strict_types=1);

namespace Zoo\Data\Domain\Trait;

use Zoo\Data\Domain\Animal;

trait GetOldTrait
{
    public function getOld(): void
    {
        $this->health -= rand(Animal::MIN_HEALTH_DECREASE, Animal::MAX_HEALTH_DECREASE);
    }
}