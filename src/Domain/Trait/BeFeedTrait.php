<?php declare(strict_types=1);

namespace Zoo\Data\Domain\Trait;

use Zoo\Data\Domain\Animal;

trait BeFeedTrait
{
    public function getFood(int $food) : void
    {
        $this->health += $food;

        $this->health = min($this->health, Animal::MAX_HEALTH);
    }

}