### To start the project you must use docker-compose:
***docker-compose up***

### Then to run compose and download the project dependencies connect to the container like this:
***docker compose exec zoo sh***

### Then run the composer command
***composer install***